package Experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.deri.iris.Configuration;
import org.deri.iris.EvaluationException;
import org.deri.iris.api.basics.IPredicate;
import org.deri.iris.api.basics.IRule;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.compiler.Parser;
import org.deri.iris.compiler.ParserException;
import org.deri.iris.evaluation.stratifiedbottomup.seminaive.SemiNaiveEvaluator;
import org.deri.iris.facts.Facts;
import org.deri.iris.facts.IFacts;
import org.deri.iris.optimisations.magicsets.MagicSets;
import org.deri.iris.optimisations.rulefilter.RuleFilter;
import org.deri.iris.rules.compiler.ICompiledRule;
import org.deri.iris.rules.compiler.RuleCompiler;
import org.deri.iris.storage.IRelation;

import TopKBasics.KeyMap2;

public class QueryByProvenanceExp 
{
	
	public static void main (String [] args)
	{
		int rowNum = 4;
		boolean isLaptop = true;
		boolean isTrio = true;
		runIRIS_prov(isLaptop, rowNum, isTrio);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: runIRIS_Prov																				
	/** Description: takes the IRIS file and runs an evaluation of the query on the DB using the top-k mechanism				 
	/*************************************************************************************************************/	
	
	private static void runIRIS_prov (boolean isLaptop, int rowNum, boolean isTrio)
	{
		File program;
		KeyMap2.getInstance().Reset();
		String path = (isLaptop) ? "C:\\Users\\user\\git\\querybyprovenance\\" : "C:\\Users\\amirgilad\\workspace\\querybyprovenance\\"; 
		deleteFile(path + "treesForQueryByProvenance.txt");
		deleteFile(path + "ProvenanceFacts.iris");
		program = new File(path + "TPC-H_DB.iris");// Create a Reader on the Datalog program file.
		Reader reader;
		try 
		{
			reader = new FileReader(program);
			// Parse the Datalog program.
			Parser parser = new Parser();
			parser.parse(reader);

			// Retrieve the facts, rules and queries from the parsed program.
			Map<IPredicate, IRelation> factMap = parser.getFacts();
			List<IRule> rules = parser.getRules();

			// Create a default configuration.
			Configuration configuration = new Configuration();

			// Enable Magic Sets together with rule filtering.
			configuration.programOptmimisers.add(new RuleFilter());
			configuration.programOptmimisers.add(new MagicSets());

			// Convert the map from predicate to relation to a IFacts object.
			Facts facts = new Facts(factMap, configuration.relationFactory);

			int initialSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
				initialSize += facts.get( predicate ).size();
			
			// Evaluate all queries over the knowledge base.
			List<ICompiledRule> cr = compile(rules, facts, configuration);
			SemiNaiveEvaluator sn = new SemiNaiveEvaluator();
			sn.evaluateRules(cr, facts, configuration);
			
			int endSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
			{
				endSize += facts.get( predicate ).size();
			}
			
			System.out.println("number of facts derived: " + (endSize - initialSize));
			
			try
			{
				boolean chooseRow;
				Random r = new Random();
				int rowsWritten = 0;
				Writer writer = new BufferedWriter(new FileWriter(path + "treesForQueryByProvenance.txt", true));//append to file
				Writer writerForRecall = new BufferedWriter(new FileWriter(path + "ProvenanceFacts.iris", true));//append to file
				
				for( Map.Entry<IPredicate, IRelation> entry : facts.getPredicateRelationMap().entrySet() )
				{
					IRelation relation = entry.getValue();

					for( int t = 0; t < relation.size(); ++t )
					{
						chooseRow = (r.nextDouble() > 0.7) ? true : false;
						
						ITuple tuple = relation.get( t );
						if (tuple.getTree() != null)
						{
							if (chooseRow && rowsWritten < rowNum)
							{
								//create example for learning
								if (isTrio)//write trio(x) provenance
								{
									writer.write(tuple.getTree().toStringProvTRIO() + "\n");
								}
								else//write N(x) provenance
								{
									writer.write(tuple.getTree() + "\n"); 
								}
								// create database for checking precision/recall
								// every line correlates to a provenance row
								writerForRecall.write(tuple.getTree().toStringProv() + "\n");
								rowsWritten++;
							}
						}
					}
				}
				
				writer.close();
				writerForRecall.close();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException | EvaluationException | ParserException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: compile																				
	/** Description: compiles the rules of IRIS				 
	/*************************************************************************************************************/	
	
	private static List<ICompiledRule> compile( List<IRule> rules, IFacts facts, Configuration mConfiguration ) throws EvaluationException
	{
		assert rules != null;
		assert facts != null;
		assert mConfiguration != null;
		
		List<ICompiledRule> compiledRules = new ArrayList<ICompiledRule>();
		
		RuleCompiler rc = new RuleCompiler( facts, mConfiguration.equivalentTermsFactory.createEquivalentTerms(), mConfiguration );

		for( IRule rule : rules )
			compiledRules.add( rc.compile( rule ) );
		
		return compiledRules;
	}
	
	
	/*************************************************************************************************************/
	/** Title: deleteFile																				
	/** Description: delete the DB file if it exists				 
	/*************************************************************************************************************/	
	
	private static void deleteFile( String path )
	{
		File file = new File( path );
		
		if(file.delete())
		{
			System.out.println(file.getName() + " is deleted");
		}
		else
		{
			System.out.println("Delete operation failed");
		}
	}
}
